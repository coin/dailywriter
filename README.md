# dailywriter

Daily writing exercises for creative writing.

### Objective
Create a site that allows writers to continue and finish short stories, given some details about the story.